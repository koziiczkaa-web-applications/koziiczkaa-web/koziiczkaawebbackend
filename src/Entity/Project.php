<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private string $img;

    #[ORM\Column(type: 'text')]
    private string $description;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $url;

    #[ORM\Column(type: 'text', nullable: true)]
    private string $git;

    #[ORM\ManyToMany(targetEntity: ProjectTechnology::class, mappedBy: 'projects', cascade: ['persist'])]
    private Collection $technologies;

    public function __construct(int $id, string $name, string $img, string $description, ?string $url, string $git)
    {
        $this->id = $id;
        $this->name = $name;
        $this->img = $img;
        $this->description = $description;
        $this->url = $url;
        $this->git = $git;
        $this->technologies = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImg(): string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getGit(): string
    {
        return $this->git;
    }

    public function setGit(?string $git): self
    {
        $this->git = $git;

        return $this;
    }

    public function getTechnologies(): ArrayCollection|Collection
    {
        return $this->technologies;
    }

    #[ArrayShape([
        "id" => "int",
        "name" => "string",
        "img" => "string",
        "description" => "string",
        "url" => "string|null",
        "git" => "string",
        "technologies" => "array"
    ])]
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "img" => $this->getImg(),
            "description" => $this->getDescription(),
            "url" => $this->getUrl(),
            "git" => $this->getGit(),
            "technologies" => $this->getTechnologies()->toArray()
        ];
    }
}
