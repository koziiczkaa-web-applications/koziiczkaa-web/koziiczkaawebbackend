<?php

namespace App\Entity;

use App\Repository\EducationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

#[ORM\Entity(repositoryClass: EducationRepository::class)]
class Education implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 63)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private string $subtitle;

    #[ORM\Column(type: 'date')]
    private DateTimeInterface $beginDate;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $endDate;

    #[ORM\Column(type: 'string', length: 255)]
    private string $degree;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    public function getBeginDate(): ?DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(DateTimeInterface $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDegree(): string
    {
        return $this->degree;
    }

    public function setDegree(string $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    #[ArrayShape([
        "id" => "int",
        "name" => "string",
        "subtitle" => "string",
        "beginDate" => "\DateTimeInterface",
        "endDate" => "\DateTimeInterface|null",
        "degree" => "string"
    ])]
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "subtitle" => $this->getSubtitle(),
            "beginDate" => $this->getBeginDate()->format("Y-m-d H:i:s"),
            "endDate" => $this->getEndDate()?->format("Y-m-d H:i:s"),
            "degree" => $this->getDegree()
        ];
    }
}
