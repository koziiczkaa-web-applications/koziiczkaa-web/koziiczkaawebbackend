<?php

namespace App\Entity;

use App\Repository\TechnologyRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;

#[ORM\Entity(repositoryClass: TechnologyRepository::class)]
class Technology implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $icon;

    #[ORM\Column(type: 'string', length: 31)]
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    #[ArrayShape([
        "id" => "int",
        "icon" => "string",
        "name" => "string"
    ])]
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->getId(),
            "icon" => $this->getIcon(),
            "name" => $this->getName()
        ];
    }
}
