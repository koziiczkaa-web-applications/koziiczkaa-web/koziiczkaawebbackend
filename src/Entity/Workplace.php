<?php

namespace App\Entity;

use App\Repository\WorkplaceRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

#[ORM\Entity(repositoryClass: WorkplaceRepository::class)]
class Workplace implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 63)]
    private string $name;

    #[ORM\Column(type: 'date')]
    private DateTimeInterface $beginDate;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $endDate;

    #[ORM\Column(type: 'string', length: 255)]
    private string $position;

    #[ORM\Column(type: 'text')]
    private string $description;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBeginDate(): DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(DateTimeInterface $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    #[ArrayShape([
        "id" => "int",
        "name" => "string",
        "beginDate" => "\DateTimeInterface",
        "endDate" => "\DateTimeInterface|null",
        "position" => "string",
        "description" => "string"
    ])]
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "beginDate" => $this->getBeginDate()->format("Y-m-d H:i:s"),
            "endDate" => $this->getEndDate()?->format("Y-m-d H:i:s"),
            "position" => $this->getPosition(),
            "description" => $this->getDescription()
        ];
    }
}
