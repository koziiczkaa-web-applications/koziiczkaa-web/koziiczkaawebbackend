<?php

namespace App\Controller;

use App\Repository\ContactRepository;
use App\Repository\EducationRepository;
use App\Repository\ProjectRepository;
use App\Repository\TechnologyRepository;
use App\Repository\WorkplaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebController extends AbstractController
{
    private TechnologyRepository $technologyRepository;
    private WorkplaceRepository $workplaceRepository;
    private EducationRepository $educationRepository;
    private ContactRepository $contactRepository;
    private ProjectRepository $projectRepository;

    /**
     * @param TechnologyRepository $technologyRepository
     * @param WorkplaceRepository $workplaceRepository
     * @param EducationRepository $educationRepository
     * @param ContactRepository $contactRepository
     * @param ProjectRepository $projectRepository
     */
    public function __construct(TechnologyRepository $technologyRepository,
                                WorkplaceRepository  $workplaceRepository,
                                EducationRepository  $educationRepository,
                                ContactRepository    $contactRepository,
                                ProjectRepository    $projectRepository)
    {
        $this->technologyRepository = $technologyRepository;
        $this->workplaceRepository = $workplaceRepository;
        $this->educationRepository = $educationRepository;
        $this->contactRepository = $contactRepository;
        $this->projectRepository = $projectRepository;
    }


    #[Route('/technology', name: 'technology')]
    public function getTechnologies(): Response
    {
        $technologies = $this->technologyRepository->findAll();
        return new JsonResponse($technologies);
    }

    #[Route('/workplace', name: 'workplace')]
    public function getWorkplaces(): Response
    {
        $workplaces = $this->workplaceRepository->findAll();
        return new JsonResponse($workplaces);
    }

    #[Route('/education', name: 'education')]
    public function getEducation(): Response
    {
        $education = $this->educationRepository->findAll();
        return new JsonResponse($education);
    }

    #[Route('/contact', name: 'contact')]
    public function getContacts(): Response
    {
        $contacts = $this->contactRepository->findAll();
        return new JsonResponse($contacts);
    }

    #[Route('/project', name: 'project')]
    public function getProjects(): Response
    {
        $projects = $this->projectRepository->findAll();
        return new JsonResponse($projects);
    }
}