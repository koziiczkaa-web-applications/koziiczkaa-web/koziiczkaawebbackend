<?php

namespace App\Repository;

use App\Entity\ProjectTechnology;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProjectTechnology>
 *
 * @method ProjectTechnology|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectTechnology|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectTechnology[]    findAll()
 * @method ProjectTechnology[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectTechnologyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectTechnology::class);
    }

    public function add(ProjectTechnology $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ProjectTechnology $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
